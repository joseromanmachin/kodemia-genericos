package com.kotlin.innerclassgenerics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter <T >(private val data : ArrayList<T>) : RecyclerView.Adapter<Adapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tarjeta_recycler, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
          holder.textView.text = data[position].toString()
    }

    override fun getItemCount() = data.size

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
         var textView : TextView = view.findViewById(R.id.textView)
    }
}