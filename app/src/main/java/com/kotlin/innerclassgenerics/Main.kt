package com.kotlin.innerclassgenerics

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Main : AppCompatActivity() {

    lateinit var recycler1: RecyclerView
    lateinit var recycler2 : RecyclerView

    val camisas = ArrayList<Camisas>()
    val zapatos = ArrayList<Zapatos>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inicializar()

        repeat(5){
            camisas.add(Camisas(it,"Camisa ${it +1}","marca"))
            zapatos.add(Zapatos(it,"Cascos ${it + 1}"))
        }


        val adatperCamisas = Adapter(camisas)
        val adapterZapatos = Adapter(zapatos)


        configRecyclerView(recycler1,adapterZapatos)
        configRecyclerView(recycler2,adatperCamisas)

    }
    private fun configRecyclerView( recycler : RecyclerView ,  adapter: Adapter<*>){
        recycler.hasFixedSize()
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
    }

    private fun inicializar() {
        recycler1 = findViewById(R.id.recycler1)
        recycler2 = findViewById(R.id.recycler2)
    }
}